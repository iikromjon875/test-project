package user

// filters

type Filter struct {
	Limit       *int
	Offset      *int
	Page        *int
	Age         *int
	Gender      *string
	Nationality *string
}

// main

type Create struct {
	Name        string `json:"name" form:"name"`
	Surname     string `json:"surname" form:"surname"`
	Patronymic  string `json:"patronymic" form:"patronymic"`
	Age         int    `json:"age" form:"age"`
	Gender      string `json:"gender" form:"gender"`
	Nationality string `json:"nationality" form:"nationality"`
}

type Update struct {
	ID          string  `json:"id" bun:"id"`
	Name        *string `json:"name" form:"name"`
	Surname     *string `json:"surname" form:"surname"`
	Patronymic  *string `json:"patronymic" form:"patronymic"`
	Age         *int    `json:"age" form:"age"`
	Gender      *string `json:"gender" form:"gender"`
	Nationality *string `json:"nationality" form:"nationality"`
}

type Detail struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Surname     string `json:"surname"`
	Patronymic  string `json:"patronymic"`
	Age         int    `json:"age"`
	Gender      string `json:"gender"`
	Nationality string `json:"nationality"`
}

type List struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Surname     string `json:"surname"`
	Patronymic  string `json:"patronymic"`
	Age         int    `json:"age"`
	Gender      string `json:"gender"`
	Nationality string `json:"nationality"`
}
