package user

import (
	"context"
	"test-project/internal/entity"
)

type Service struct {
	repo Repository
}

func (s Service) GetAll(ctx context.Context, filter Filter) ([]entity.User, int, error) {
	return s.repo.GetAll(ctx, filter)
}

func (s Service) GetDetail(ctx context.Context, id string) (entity.User, error) {
	return s.repo.GetDetail(ctx, id)
}

func (s Service) Create(ctx context.Context, data Create) (entity.User, error) {
	return s.repo.Create(ctx, data)
}

func (s Service) Update(ctx context.Context, data Update) error {
	return s.repo.Update(ctx, data)
}

func (s Service) Delete(ctx context.Context, id string) error {
	return s.repo.Delete(ctx, id)
}

func NewService(repo Repository) *Service {
	return &Service{repo}
}
