package user

import (
	"context"
	"test-project/internal/entity"
)

type Repository interface {
	GetAll(ctx context.Context, filter Filter) ([]entity.User, int, error)
	GetDetail(ctx context.Context, id string) (entity.User, error)
	Create(ctx context.Context, data Create) (entity.User, error)
	Update(ctx context.Context, data Update) error
	Delete(ctx context.Context, id string) error
}
