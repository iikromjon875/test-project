package user

import (
	"context"
	"test-project/internal/entity"
	"test-project/internal/service/user"
)

type User interface {
	GetAll(ctx context.Context, filter user.Filter) ([]entity.User, int, error)
	GetDetail(ctx context.Context, id string) (entity.User, error)
	Create(ctx context.Context, data user.Create) (entity.User, error)
	Update(ctx context.Context, data user.Update) error
	Delete(ctx context.Context, id string) error
}
