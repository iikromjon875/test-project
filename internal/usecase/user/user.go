package user

import (
	"context"
	"test-project/internal/entity"
	"test-project/internal/service/user"
)

type UseCase struct {
	user User
}

func NewUseCase(user User) *UseCase {
	return &UseCase{
		user,
	}
}

func (uu UseCase) GetUserList(ctx context.Context, filter user.Filter) ([]user.List, int, error) {
	data, count, err := uu.user.GetAll(ctx, filter)
	if err != nil {
		return nil, 0, err
	}

	var list []user.List

	for _, u := range data {
		var detail user.List

		detail.ID = u.ID
		detail.Name = u.Name
		detail.Surname = u.Surname
		detail.Patronymic = u.Patronymic
		detail.Age = u.Age
		detail.Gender = u.Gender
		detail.Nationality = u.Nationality

		list = append(list, detail)
	}

	return list, count, nil
}

func (uu UseCase) GetUserDetail(ctx context.Context, id string) (user.Detail, error) {
	data, err := uu.user.GetDetail(ctx, id)
	if err != nil {
		return user.Detail{}, err
	}

	var detail user.Detail

	detail.ID = data.ID
	detail.Name = data.Name
	detail.Surname = data.Surname
	detail.Patronymic = data.Patronymic
	detail.Age = data.Age
	detail.Gender = data.Gender
	detail.Nationality = data.Nationality

	return detail, nil
}

func (uu UseCase) CreateUser(ctx context.Context, data user.Create) (entity.User, error) {
	detail, err := uu.user.Create(ctx, data)
	if err != nil {
		return entity.User{}, err
	}

	return detail, nil
}

func (uu UseCase) UpdateUser(ctx context.Context, data user.Update) error {
	return uu.user.Update(ctx, data)
}

func (uu UseCase) DeleteUser(ctx context.Context, id string) error {
	return uu.user.Delete(ctx, id)
}
