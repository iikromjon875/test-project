CREATE TABLE users
(
    id          text not null unique,
    name        text,
    surname     text,
    patronymic  text,
    age         integer,
    gender      text,
    nationality text
);
