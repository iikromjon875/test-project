package entity

import "github.com/uptrace/bun"

type User struct {
	bun.BaseModel `bun:"table:users"`

	ID          string `json:"id" bun:"id"`
	Name        string `json:"name" bun:"name"`
	Surname     string `json:"surname" bun:"surname"`
	Patronymic  string `json:"patronymic" bun:"patronymic"`
	Age         int    `json:"age" bun:"age"`
	Gender      string `json:"gender" bun:"gender"`
	Nationality string `json:"nationality" bun:"nationality"`
}
