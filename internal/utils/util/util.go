package util

import (
	"errors"
	"log"
	"net/http"
	"reflect"
)

type FieldError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

type Error struct {
	Err    error
	Status int
	Fields []FieldError
}

func (e *Error) Error() string {
	return e.Err.Error()
}

func ValidateStruct(s interface{}, requiredFields ...string) error {
	structVal := reflect.Value{}
	if reflect.Indirect(reflect.ValueOf(s)).Kind() == reflect.Struct {
		structVal = reflect.Indirect(reflect.ValueOf(s))
	} else {
		return errors.New("input param should be a struct")
	}

	errFields := make([]FieldError, 0)

	structType := reflect.Indirect(reflect.ValueOf(s)).Type()
	fieldNum := structVal.NumField()

	for i := 0; i < fieldNum; i++ {
		field := structVal.Field(i)
		fieldName := structType.Field(i).Name

		isSet := field.IsValid() && !field.IsZero()
		if !isSet {
			log.Print(isSet, fieldName, reflect.ValueOf(field))
			for _, f := range requiredFields {
				if f == fieldName {
					errFields = append(errFields, FieldError{
						Error: "field is required!",
						Field: fieldName,
					})
				}
			}
		}
	}

	if len(errFields) > 0 {
		return &Error{
			Err:    errors.New("required fields"),
			Fields: errFields,
			Status: http.StatusBadRequest,
		}
	}

	return nil
}
