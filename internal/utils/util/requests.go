package util

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// structs

type Age struct {
	Age int `json:"age"`
}

type Gender struct {
	Gender string `json:"gender"`
}

type Nationality struct {
	Country []struct {
		CountryID   string  `json:"country_id"`
		Probability float32 `json:"probability"`
	} `json:"country"`
}

// --- functions

func GetAgeRequest(url string) (int, error) {
	var data Age

	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return 0, err
	}

	return data.Age, nil
}

func GetGenderRequest(url string) (string, error) {
	var data Gender

	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return "", err
	}

	return data.Gender, nil
}

func GetNationalityRequest(url string) (string, error) {
	var data Nationality

	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return "", err
	}

	country := findMaxProbabilityCountryID(data)

	return country, nil
}

func findMaxProbabilityCountryID(n Nationality) string {
	if len(n.Country) == 0 {
		return "" // Return an empty string if the slice is empty
	}

	maxProbability := n.Country[0].Probability
	maxProbabilityCountryID := n.Country[0].CountryID

	for _, country := range n.Country {
		if country.Probability > maxProbability {
			maxProbability = country.Probability
			maxProbabilityCountryID = country.CountryID
		}
	}

	return maxProbabilityCountryID
}
