package user

import (
	"context"
	"github.com/google/uuid"
	"github.com/uptrace/bun"
	"test-project/internal/entity"
	"test-project/internal/service/user"
)

type Repository struct {
	*bun.DB
}

func (r Repository) GetAll(ctx context.Context, filter user.Filter) ([]entity.User, int, error) {
	var list []entity.User

	q := r.NewSelect().Model(&list)

	if filter.Age != nil {
		q.WhereGroup(" and ", func(query *bun.SelectQuery) *bun.SelectQuery {
			query.Where("age = ?", *filter.Age)
			return query
		})
	}
	if filter.Nationality != nil {
		q.WhereGroup(" and ", func(query *bun.SelectQuery) *bun.SelectQuery {
			query.Where("lower(nationality) ilike lower(?)", "%"+*filter.Nationality+"%")
			return query
		})
	}
	if filter.Gender != nil {
		q.WhereGroup(" and ", func(query *bun.SelectQuery) *bun.SelectQuery {
			query.Where("gender = ?", *filter.Gender)
			return query
		})
	}

	if filter.Page != nil && filter.Limit != nil {
		offset := (*filter.Page - 1) * (*filter.Limit)
		filter.Offset = &offset
	}
	if filter.Limit != nil {
		q.Limit(*filter.Limit)
	}
	if filter.Offset != nil {
		q.Offset(*filter.Offset)
	}

	count, err := q.ScanAndCount(ctx)

	return list, count, err
}

func (r Repository) GetDetail(ctx context.Context, id string) (entity.User, error) {
	var detail entity.User

	err := r.NewSelect().Model(&detail).Where("id = ?", id).Scan(ctx)

	return detail, err
}

func (r Repository) Create(ctx context.Context, data user.Create) (entity.User, error) {
	detail := entity.User{
		ID:          uuid.New().String(),
		Name:        data.Name,
		Surname:     data.Surname,
		Patronymic:  data.Patronymic,
		Age:         data.Age,
		Gender:      data.Gender,
		Nationality: data.Nationality,
	}

	_, err := r.NewInsert().Model(&detail).Exec(ctx)

	return detail, err
}

func (r Repository) Update(ctx context.Context, data user.Update) error {
	var detail entity.User

	q := r.NewUpdate().Model(&detail).Where("id = ?", data.ID)

	if data.Name != nil {
		q.Set("name = ?", data.Name)
	}
	if data.Surname != nil {
		q.Set("surname = ?", data.Surname)
	}
	if data.Patronymic != nil {
		q.Set("patronymic = ?", data.Patronymic)
	}
	if data.Age != nil {
		q.Set("age = ?", data.Age)
	}
	if data.Gender != nil {
		q.Set("gender = ?", data.Gender)
	}
	if data.Nationality != nil {
		q.Set("nationality = ?", data.Nationality)
	}

	_, err := q.Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (r Repository) Delete(ctx context.Context, id string) error {
	_, err := r.NewDelete().Table("users").Where("id = ?", id).Exec(ctx)

	return err
}

func NewRepository(DB *bun.DB) *Repository {
	return &Repository{
		DB,
	}
}
