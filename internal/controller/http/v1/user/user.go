package user

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"strconv"
	user_service "test-project/internal/service/user"
	"test-project/internal/usecase/user"
	"test-project/internal/utils/util"
)

type Controller struct {
	useCase *user.UseCase
}

func NewController(useCase *user.UseCase) *Controller {
	return &Controller{useCase: useCase}
}

// #user

func (uc Controller) GetUserList(c *gin.Context) {
	var filter user_service.Filter
	query := c.Request.URL.Query()

	limitQ := query["limit"]
	if len(limitQ) > 0 {
		queryInt, err := strconv.Atoi(limitQ[0])
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":  "Limit must be number!",
				"status": false,
			})

			return
		}

		filter.Limit = &queryInt
	}

	offsetQ := query["offset"]
	if len(offsetQ) > 0 {
		queryInt, err := strconv.Atoi(offsetQ[0])
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":  "Offset must be number!",
				"status": false,
			})

			return
		}

		filter.Offset = &queryInt
	}

	pageQ := query["page"]
	if len(pageQ) > 0 {
		queryInt, err := strconv.Atoi(pageQ[0])
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":  "Offset must be number!",
				"status": false,
			})

			return
		}

		filter.Page = &queryInt
	}

	nameQ := query["age"]
	if len(nameQ) > 0 {
		queryInt, err := strconv.Atoi(nameQ[0])
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":  "age must be number!",
				"status": false,
			})

			return
		}

		filter.Age = &queryInt
	}

	genderQ := query["gender"]
	if len(genderQ) > 0 {
		filter.Gender = &genderQ[0]
	}

	nationalityQ := query["nationality"]
	if len(nationalityQ) > 0 {
		filter.Nationality = &nationalityQ[0]
	}

	ctx := context.Background()

	list, count, err := uc.useCase.GetUserList(ctx, filter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":  err.Error(),
			"status": false,
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"error":  nil,
		"status": true,
		"data": map[string]interface{}{
			"results": list,
			"count":   count,
		},
	})
}

func (uc Controller) GetUserDetail(c *gin.Context) {
	id := c.Param("id")

	if _, err := uuid.Parse(id); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":  "Give a proper ID!",
			"status": false,
		})

		return
	}

	ctx := context.Background()

	detail, err := uc.useCase.GetUserDetail(ctx, id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":  err.Error(),
			"status": false,
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"error":  nil,
		"status": true,
		"data":   detail,
	})
}

func (uc Controller) CreateUser(c *gin.Context) {
	var data user_service.Create

	if err := c.ShouldBind(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":  err.Error(),
			"status": false,
		})

		return
	}

	err := util.ValidateStruct(data, "Name", "Surname")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":  err,
			"status": false,
		})

		return
	}

	// -- age ----------------------------------------

	resAgeData, err := util.GetAgeRequest(fmt.Sprintf(
		"https://api.agify.io/?name=%s",
		data.Name,
	))
	data.Age = resAgeData

	// -- gender --------------------------------------

	resGenderData, err := util.GetGenderRequest(fmt.Sprintf(
		"https://api.genderize.io/?name=%s",
		data.Name,
	))

	data.Gender = resGenderData

	// -- gender --------------------------------------

	resNationalityData, err := util.GetNationalityRequest(fmt.Sprintf(
		"https://api.nationalize.io/?name=%s",
		data.Name,
	))

	data.Nationality = resNationalityData

	ctx := context.Background()
	detail, err := uc.useCase.CreateUser(ctx, data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":  err.Error(),
			"status": false,
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"error":  nil,
		"status": true,
		"data":   detail,
	})
}

func (uc Controller) UpdateUser(c *gin.Context) {
	id := c.Param("id")

	if _, err := uuid.Parse(id); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":  "Give a proper ID!",
			"status": false,
		})

		return
	}

	var data user_service.Update

	if err := c.ShouldBind(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":  err.Error(),
			"status": false,
		})

		return
	}

	err := util.ValidateStruct(data, "ID")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":  err,
			"status": false,
		})

		return
	}

	ctx := context.Background()

	err = uc.useCase.UpdateUser(ctx, data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":  err.Error(),
			"status": false,
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"error":  nil,
		"status": true,
	})
}

func (uc Controller) DeleteUser(c *gin.Context) {
	id := c.Param("id")

	if _, err := uuid.Parse(id); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":  "Give a proper ID!",
			"status": false,
		})

		return
	}

	ctx := context.Background()

	err := uc.useCase.DeleteUser(ctx, id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":  err.Error(),
			"status": false,
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"error":  nil,
		"status": true,
	})
}
