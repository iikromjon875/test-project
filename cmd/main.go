package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"log"
	user4 "test-project/internal/controller/http/v1/user"
	"test-project/internal/pkg/repository/postgres"
	"test-project/internal/repository/postgres/user"
	user2 "test-project/internal/service/user"
	user3 "test-project/internal/usecase/user"
	"time"
)

func main() {
	r := gin.Default()

	// cors
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return true
		},
		MaxAge: 12 * time.Hour,
	}))

	// database
	postgresDB := postgres.NewPostgres()

	// repository
	userRepo := user.NewRepository(postgresDB)

	// service
	userService := user2.NewService(userRepo)

	// use-case
	userUseCase := user3.NewUseCase(userService)

	// controller
	userController := user4.NewController(userUseCase)

	// basics
	baseApi := r.Group("api/v1")

	// user
	{
		baseApi.GET("/user/list", userController.GetUserList)
		baseApi.GET("/user/:id", userController.GetUserDetail)
		baseApi.POST("/user/create", userController.CreateUser)
		baseApi.PUT("/user/:id", userController.UpdateUser)
		baseApi.DELETE("/user/:id", userController.DeleteUser)
	}

	log.Fatalln(r.Run(":8001"))
}
